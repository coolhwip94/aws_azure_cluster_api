provider "kubernetes" {
  config_path    = "~/.kube/config"
  config_context = "kubernetes-admin@kubernetes"
}


resource "kubernetes_deployment" "fast-api-app" {
  metadata {
    name = "fast-api-app"
    labels = {
      app = "fast-api-app"
    }
  }

  spec {
    replicas          = 3
    min_ready_seconds = 5

    selector {
      match_labels = {
        app = "fast-api-app"
      }
    }

    strategy {
      type = "RollingUpdate"
      rolling_update {
        max_unavailable = 0
        max_surge       = 1
      }
    }

    template {
      metadata {
        labels = {
          app = "fast-api-app"
        }
      }

      spec {
        container {
          image = "registry.gitlab.com/coolhwip94/aws_azure_cluster_api:latest"
          name  = "flask-api-app"
          port {
            container_port = 80
          }

        }
      }
    }
  }
}

resource "kubernetes_service" "fast-api-svc" {
  metadata {
    name = "fast-api-svc"
    labels = {
      app = "fast-api-svc"
    }
  }
  spec {
    selector = {
      app = "fast-api-app"
    }
    port {
      port        = 80
      node_port = 30051
    }

    type = "NodePort"
  }
}