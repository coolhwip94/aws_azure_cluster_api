terraform {
  required_providers {
    azurerm = {
      source = "hashicorp/azurerm"
    }
  }
}

provider "azurerm" {
  subscription_id = var.subscription_id
  client_id       = var.client_id
  client_secret   = var.client_secret
  tenant_id       = var.tenant_id

  features {}
}

module "cluster" {
  source                = "./modules/cluster/"
  serviceprinciple_id   = var.client_id
  serviceprinciple_key  = var.client_secret
#   ssh_key               = var.ssh_key
  location              = var.location
#   kubernetes_version    = var.kubernetes_version  
  
}

module "k8s" {
  source                = "./modules/k8s/"
  host                  = "${module.cluster.host}"
  client_certificate    = "${base64decode(module.cluster.client_certificate)}"
  client_key            = "${base64decode(module.cluster.client_key)}"
  cluster_ca_certificate= "${base64decode(module.cluster.cluster_ca_certificate)}"
}


output "loadbalancer_hostname" {
    value = "${module.k8s.loadbalancer_hostname}"
}