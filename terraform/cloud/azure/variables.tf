
variable "subscription_id" {
  description = "subscription_id"
}

variable "client_id" {
  description = "client id/app id"
}

variable "client_secret" {
  description = "client_secret"
}

variable "tenant_id" {
  description = "tenant_id"
}

variable "location" {
  description = "localtion of server"
}


