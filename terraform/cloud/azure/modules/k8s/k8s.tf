provider "kubernetes" {
    host                   =  var.host
    client_certificate     =  var.client_certificate
    client_key             =  var.client_key
    cluster_ca_certificate =  var.cluster_ca_certificate
}


resource "kubernetes_deployment" "fast-api-app" {
  metadata {
    name = "fast-api-app"
    labels = {
      app = "fast-api-app"
    }
  }

  spec {
    replicas          = 2
    min_ready_seconds = 5

    selector {
      match_labels = {
        app = "fast-api-app"
      }
    }

    strategy {
      type = "RollingUpdate"
      rolling_update {
        max_unavailable = 0
        max_surge       = 1
      }
    }

    template {
      metadata {
        labels = {
          app = "fast-api-app"
        }
      }

      spec {
        container {
          image = "registry.gitlab.com/coolhwip94/aws_azure_cluster_api:latest"
          name  = "flask-api-app"
          port {
            container_port = 80
          }

          resources {
            limits = {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "fast-api-svc" {
  metadata {
    name = "fast-api-svc"
    labels = {
      app = "fast-api-svc"
    }
  }
  spec {
    selector = {
      app = "fast-api-app"
    }
    port {
      port        = 80
      target_port = 80
    }

    type = "LoadBalancer"
  }
}

output "loadbalancer_hostname" {
  value = kubernetes_service.fast-api-svc.status.0.load_balancer.0.ingress.0.ip
}