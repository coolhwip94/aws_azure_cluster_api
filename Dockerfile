FROM python:3.6

WORKDIR /app
ADD main.py requirements.txt /app/
RUN pip install -r requirements.txt
ENTRYPOINT ["uvicorn", "main:app", "--reload", "--port", "80", "--host", "0.0.0.0"]
