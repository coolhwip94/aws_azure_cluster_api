from fastapi import FastAPI
import os
import json
app = FastAPI()

@app.get("/")
async def root():
  return {"message": "Hello World"}

@app.get("/sysinfo")
async def sysinfo():
  return {"message": os.uname()}
