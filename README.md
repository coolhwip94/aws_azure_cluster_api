# AWS_AZURE_CLUSTER_API
> This is meant to host kubernetes examples for running a fast api within a kubernetes cluster.  
> Examples will include deploying to a local cluster, and also utilizing terraform to deploy to AWS and Azure as well.  
> Any exposed credentials will be expired after lab has been run.


## FastAPI
---
This repo utilizes a simple FastAPI python script to showcase examples.

- `main.py` : python script to run FastAPI instance
> Here is a short setup to run FastAPI
```
# Set up python virtual env
python -m venv venv

# activate
source venv/bin/activate

# install
pip install -r requirements.txt

# running
uvicorn main:app --reload --port 5001 --host 0.0.0.0

# visit your api via web browser
http://<ip_or_hostname>:5001
```

- `Dockerfile` : Defines image that utilizes the FastAPI script here

The image has already been built and pushed up to this git repo's registry, and can be used by referencing this image : `registry.gitlab.com/coolhwip94/aws_azure_cluster_api`


## Installing Terraform
> Terraform will be used to deploy to our kubernetes clusters
---
Install Terraform
```
brew tap hashicorp/tap
brew install hashicorp/tap/terraform
```

## Local Deploy
---
> Local simply means it will use your settings from `~/.kube/config` this can be configured to manage any kubernetes cluster even remote ones.  
> These examples skip the creation portion of the cluster, and just focus on deploying to one.
- `local_deploy.yml` : this is a kubernetes configuration that can be used to deploy the example FastAPI to a kubernetes cluster.
```
# creating our deployment and service
kubectl apply -f local_deploy.yml

# destroying/deleting
kubectl delete -f local_deploy.yml
```

- `terraform/local/main.tf` : terraform configuration for deploying the same FastAPI app to a kubernetes cluster
> Configure Kubernetes provider according to your config `~/.kube/config`
```
provider "kubernetes" {
  config_path    = "~/.kube/config"
  config_context = "kubernetes-admin@kubernetes"
}
```

Applying our config
```
# initialize terraform
terraform init

# output plan
terraform plan -outplan <filename>.tfplan

# apply plan
terraform apply <filename>.tfplan

# delete our service and deployment
terraform destroy
```


## Cloud Deploy
---
> Deploying to cloud based kubernetes instances will be done utilizing terraform.

### AWS - EKS | Deployment | Service
---
> Terraform will be utilized to create a kubernetes cluster and deploy the docker image with FastAPI.  

Configure variables in `cloud/aws/terraform.tfvars`
> Here is an example
```
# IAM Access and Secret Key for your IAM user
aws_access_key = "ABC123456"
aws_secret_key = "ABC123456"
cluster_name   = "fast-api-cluster"
map_accounts = ["1234AccountNumber"]
map_roles = [
    {
      rolearn  = "arn:aws:iam::1234567:root"
      username = "role1"
      groups   = ["system:masters"]
    },
]
region = "us-east-1"
```

> Note : You need to be in the `terraform/cloud/aws` diretory to run the following commands. 
```
# Initialize Terraform
terraform init

# Output Plan

terraform plan -out deploy.tfplan


# Apply plan (creates cluster and kubernets deployment + service)
terraform apply deploy.tfplan
```

Visit API in web browser
> After applying the plan and every resource has been created, terraform will output `loadbalancer_hostname` which will  
> be the fqdn for the load balancer
```
# Swagger UI (configured for port 80)
http://<loadbalancer_hostname>/docs 
```

### AWS - EKS | Deployment | Service
---
Terraform will be utilized to deploy Azure cluster.
Configure variables in `cloud/aws/terraform.tfvars`
> Here is an example  
> `client_id` is the service principal ID and `client_secret` is the service principal key
```
subscription_id = "abc123"
client_id       = "abc123"
client_secret   = "abc123"
tenant_id       = "abc123"
location        = "West Us"
```

> Note : You need to be in the `terraform/cloud/azure` diretory to run the following commands. 
```
# Initialize Terraform
terraform init

# Output Plan

terraform plan -out deploy.tfplan


# Apply plan (creates cluster and kubernets deployment + service)
terraform apply deploy.tfplan
```

Visit API in web browser
> After applying the plan and every resource has been created, terraform will output `loadbalancer_hostname` which will  
> be the ip for the load balancer
```
# Swagger UI (configured for port 80)
http://<loadbalancer_hostname_or_ip>/docs 
```

## References
---
- https://www.youtube.com/watch?v=Qy2A_yJH5-o&t=331s (setting up kubernetes cluster in aws)
- https://registry.terraform.io/providers/hashicorp/kubernetes/latest/docs/resources/service
- https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_cluster
